import React from "react"
const CatInputs = (props) => {
  return (
    props.cats.map((val, idx)=> {
      let catId = `cat-${idx}`, ageId = `age-${idx}`, removeButton = `remove-${idx}`;
      console.log(props.cats);
      return (
        <div key={idx}>
          <label htmlFor={catId}>{`Cat #${idx + 1}`}</label>
          <input
            type="text"
            name={catId}
            data-id={idx}
            id={catId}
            value={props.cats[idx].name} 
            className="name"
          />
          <label htmlFor={ageId}>Age</label>
          <input
            type="text"
            name={ageId}
            data-id={idx}
            id={ageId}
            value={props.cats[idx].age} 
            className="age"
          />
          {
          (props.cats.length > 1) && 
          <input
            type="button"
            name={removeButton}
            data-id={idx}
            id={removeButton}
            value='Remove' 
            className="remove"
            onClick={ () => {props.removeHandler(idx)} }
          />
        }
        </div>
        
      )
    })
  )
}
export default CatInputs