import React from "react"
import CatInputs from "./CatInputs";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  });

class Form extends React.Component {
  state = {
    cats: [{name:"", age:""}],
    owner: "",
    description: ""
  }
  handleChange = (e) => {
      if (["name", "age"].includes(e.target.className) ) {
        let cats = [...this.state.cats]
        cats[e.target.dataset.id][e.target.className] = e.target.value.toUpperCase()
        this.setState({ cats }, () => console.log(this.state.cats))
      } else {
        this.setState({ [e.target.name]: e.target.value.toUpperCase() })
      }
    }
  addCat = (e) => {
    this.setState((prevState) => ({
      cats: [...prevState.cats, {name:"", age:""}],
    }));
  }

  handleSubmit = (e) => { e.preventDefault() 
  
    console.log("data: ", this.state);
  }

  removeHandler = (idx) => {
    //alert(idx);
    const oldCats = this.state.cats;
    console.log('length : ', oldCats.length);
    /* if(oldCats.length > 1)
      oldCats.splice(idx, 1);

    this.setState((prevState) => ({
      cats: oldCats
    })); */

    this.setState({
      cats: oldCats.filter((_, i) => i !== idx)
    });
  }
   
render() {
    let {owner, description, cats} = this.state;
    const { classes } = this.props;
    return (
        <Grid item xs={8} center style={{ margin: 'auto auto'}}>
          <Paper className={classes.paper}>
            <form onSubmit={this.handleSubmit} onChange={this.handleChange} >
                {/* <Grid container xs={12} spacing={15}>
                        <TextField
                            id="standard-name"
                            label="Name"
                            className={classes.textField}
                            value={this.state.owner}
                            onChange={this.handleChange('name')}
                            margin="normal"
                            />
                        <AddCircleIcon onClick={this.addCat}/>
                    
                </Grid> */}
                <label htmlFor="name">Owner</label> 
                <input type="text" name="owner" id="owner" value={owner} />
                <label htmlFor="description">Description</label> 
                <input type="text" name="description" id="description" value={description} />
                <CatInputs cats={cats} removeHandler= {this.removeHandler}/>
                <AddCircleIcon onClick={this.addCat}/>
                <input type="submit" value="Submit" /> 
            </form>
          </Paper>
        </Grid>
    )
  }
}

Form.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(Form);